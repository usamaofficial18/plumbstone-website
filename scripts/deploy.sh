#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_WEB=$(helm ls --namespace plumbstone -q | grep plumbstone-web )
if [ "$CHECK_WEB" = "plumbstone-web" ]
then
    echo "Updating existing plumbstone-web . . ."
    helm upgrade plumbstone-web \
        --namespace plumbstone \
        --reuse-values \
        helm-chart/plumbstone-web
fi
